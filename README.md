# HW timestamp utility

Simple program that uses Linux sockets to configure HW timestamping functionalities of the NIC. It calculates the difference between TX and RX HW timestamps and saves the result in an output file.

