/*
 * This program demonstrates how the various time stamping features in
 * the Linux kernel work. It emulates the behavior of a PTP
 * implementation in stand-alone master mode by sending PTPv1 Sync
 * multicasts once every second. It looks for similar packets, but
 * beyond that doesn't actually implement PTP.
 *
 * Outgoing packets are time stamped with SO_TIMESTAMPING with or
 * without hardware support.
 *
 * Incoming packets are time stamped with SO_TIMESTAMPING with or
 * without hardware support, SIOCGSTAMP[NS] (per-socket time stamp) and
 * SO_TIMESTAMP[NS].
 *
 * Copyright (C) 2009 Intel Corporation.
 * Author: Patrick Ohly <patrick.ohly@intel.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. * See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <asm/types.h>
#include <linux/net_tstamp.h>
#include <linux/errqueue.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>

#include <pcap.h>

#ifndef SO_TIMESTAMPING
# define SO_TIMESTAMPING         37
# define SCM_TIMESTAMPING        SO_TIMESTAMPING
#endif
#ifndef SO_TIMESTAMPNS
# define SO_TIMESTAMPNS 35
#endif
#ifndef SIOCGSTAMPNS
# define SIOCGSTAMPNS 0x8907
#endif
#ifndef SIOCGSTAMP
# define SIOCGSTAMP 0x8902
#endif
#ifndef SIOCSHWTSTAMP
# define SIOCSHWTSTAMP 0x89b0
#endif

int npkts = 0;
char fname[] = "res.txt";
FILE *fptr = NULL; 

static void usage(const char *error)
{
	if (error)
		printf("invalid option: %s\n", error);
	    
    printf("sudo tulustamp <iface> <pkt.pcap> [filter]\n\n"
           "    filter can be:\n"
           "        - 0(default): filter all (for mellanox)\n"
           "        - 1: filter PTPv2 event packet (for intel)\n");
	exit(1);
}

static void bail(const char *error)
{
	printf("%s: %s\n", error, strerror(errno));
	exit(1);
}

static const unsigned char udp_pkt[] = {
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x3c, 0xec, 0xef, 0x0c, 0xde, 0x60, 0x08, 0x00, 
    0x45, 0x00, 0x00, 0x32, 0x00, 0x01, 0x00, 0x00, 0x40, 0x11, 0xa9, 0x9e, 0x08, 0x08, 
    0x08, 0x08, 0xc0, 0xa8, 0x00, 0x64, 0x19, 0x49, 0x04, 0x49, 0x00, 0x1e, 0xb4, 0x9b, 
    0x73, 0x75, 0x62, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x75, 0x62, 0x73, 0x70, 0x61, 
    0x63, 0x65, 0x58, 0x58, 0x58, 0x58, 0x58, 0x58
};

void green() { printf("\033[1;32m"); }
void reset_color() { printf("\033[0m"); }

static long printpacket(struct msghdr *msg, int res,
			char *data,
			int sock1, int recvmsg_flags,
			int siocgstamp, int siocgstampns)
{
	struct sockaddr_in *from_addr = (struct sockaddr_in *)msg->msg_name;
	struct cmsghdr *cmsg;
	struct timeval tv;
	struct timespec ts;
	struct timeval now;
	gettimeofday(&now, 0);
	printf("%ld.%06ld: received %s data, %d bytes from %s, %zu bytes control messages\n",
	       (long)now.tv_sec, (long)now.tv_usec,
	       (recvmsg_flags & MSG_ERRQUEUE) ? "error" : "regular",
	       res,
	       inet_ntoa(from_addr->sin_addr),
	       msg->msg_controllen);
	for (cmsg = CMSG_FIRSTHDR(msg);
	     cmsg;
	     cmsg = CMSG_NXTHDR(msg, cmsg)) {
		printf("   cmsg len %zu: ", cmsg->cmsg_len);
		switch (cmsg->cmsg_level) {
		case SOL_SOCKET:
			printf("SOL_SOCKET ");
			switch (cmsg->cmsg_type) {
			case SO_TIMESTAMP: {
				struct timeval *stamp =
					(struct timeval *)CMSG_DATA(cmsg);
				printf("SO_TIMESTAMP %ld.%06ld",
				       (long)stamp->tv_sec,
				       (long)stamp->tv_usec);
				break;
			}
			case SO_TIMESTAMPNS: {
				struct timespec *stamp =
					(struct timespec *)CMSG_DATA(cmsg);
				printf("SO_TIMESTAMPNS %ld.%09ld",
				       (long)stamp->tv_sec,
				       (long)stamp->tv_nsec);
				break;
			}
			case SO_TIMESTAMPING: {
				struct timespec *stamp =
					(struct timespec *)CMSG_DATA(cmsg);
				printf("SO_TIMESTAMPING ");
				printf("SW %ld.%09ld ",
				       (long)stamp->tv_sec,
				       (long)stamp->tv_nsec);
				stamp++;
				/* skip deprecated HW transformed */
				stamp++;
				printf("HW raw %ld.%09ld\n",
				       (long)stamp->tv_sec,
				       (long)stamp->tv_nsec);
				return (long)stamp->tv_nsec;
				break;
			}
			default:
				printf("type %d", cmsg->cmsg_type);
				break;
			}
			break;
		default:
			printf("level %d type %d",
				cmsg->cmsg_level,
				cmsg->cmsg_type);
			break;
		}
		printf("\n");
	}
	if (siocgstamp) {
		if (ioctl(sock1, SIOCGSTAMP, &tv))
			printf("   %s: %s\n", "SIOCGSTAMP", strerror(errno));
		else
			printf("SIOCGSTAMP %ld.%06ld\n",
			       (long)tv.tv_sec,
			       (long)tv.tv_usec);
	}
	if (siocgstampns) {
		if (ioctl(sock1, SIOCGSTAMPNS, &ts))
			printf("   %s: %s\n", "SIOCGSTAMPNS", strerror(errno));
		else
			printf("SIOCGSTAMPNS %ld.%09ld\n",
			       (long)ts.tv_sec,
			       (long)ts.tv_nsec);
	}
}

static long recvpacket(int sock1, int recvmsg_flags,
		       int siocgstamp, int siocgstampns)
{
	char data[256];
	struct msghdr msg;
	struct iovec entry;
	struct sockaddr_in from_addr;
	struct {
		struct cmsghdr cm;
		char control[512];
	} control;
	int res;
	memset(&msg, 0, sizeof(msg));
	msg.msg_iov = &entry;
	msg.msg_iovlen = 1;
	entry.iov_base = data;
	entry.iov_len = sizeof(data);
	msg.msg_name = (caddr_t)&from_addr;
	msg.msg_namelen = sizeof(from_addr);
	msg.msg_control = &control;
	msg.msg_controllen = sizeof(control);
	res = recvmsg(sock1, &msg, recvmsg_flags|MSG_DONTWAIT);
	if (res < 0) {
		printf("%s %s: %s\n",
		       "recvmsg",
		       (recvmsg_flags & MSG_ERRQUEUE) ? "error" : "regular",
		       strerror(errno));
	} else {
		return printpacket(&msg, res, data,
			    sock1, recvmsg_flags,
			    siocgstamp, siocgstampns);
	}
}


static int load_pcap(char *file_name, u_char **buf) {
	pcap_t *p;
	char errbuf[PCAP_ERRBUF_SIZE];
	const u_char *pkt_ptr;
	struct pcap_pkthdr *hdr;

	p = pcap_open_offline(file_name, errbuf);
	if (p == NULL) {
		fprintf(stderr, "pcap_open_offline failed: %s\n", errbuf);
		return -1;
	}

	/*if (pcap_loop(fp, pktHandler, NULL) < 0) {
		fprintf(stderr, "pcaploop failed\n");
		return -1;
	}*/

	if (pcap_next_ex(p, &hdr, &pkt_ptr) < 0) {
		fprintf(stderr, "pcap_next failed\n");
		return -1;
	}

	*buf = malloc(hdr->len);

	memcpy(*buf, pkt_ptr, hdr->len);

	return hdr->len;
}

int main(int argc, char **argv)
{
	int so_timestamping_flags = SOF_TIMESTAMPING_TX_HARDWARE |
 					SOF_TIMESTAMPING_RAW_HARDWARE | 
					SO_TIMESTAMPNS |
					SOF_TIMESTAMPING_RX_HARDWARE;
	int so_timestamp = 0;
	int so_timestampns = 0;
	int siocgstamp = 0;
	int siocgstampns = 0;
	int ip_multicast_loop = 0;
	char *iface1;
	int i;
	int enabled = 1;
	int sock1;
	struct ifreq device;
	struct ifreq hwtstamp;
	struct hwtstamp_config hwconfig;
	struct sockaddr_in addr;
	struct ip_mreq imr;
	struct in_addr iaddr;
	int val;
	socklen_t len;
	struct timeval next;
	u_char *pkt = NULL;
	int pkt_len = 0;
	char *pcap_fname;
    int filter_mode = HWTSTAMP_FILTER_ALL;
    
    if(geteuid() != 0) {
        printf("tulustamp requires root priviledges!\n");
        exit(-1);
    }

	if (argc < 3)
		usage(0);

	iface1 = argv[1];
	pcap_fname = argv[2];

    if (argc == 4) {
        if (argv[4] == "1") {
            filter_mode = HWTSTAMP_FILTER_PTP_V1_L4_SYNC;
        }
    }

	pkt_len = load_pcap(pcap_fname, &pkt);
	if (pkt_len < 0)
		bail("cannot load pcap");

	printf("##### Packet ---> \n");
	for (i=0; i<pkt_len; i++) {
		printf("%02x", pkt[i]);
	}
	printf("\n");

	fptr = fopen(fname, "w");

	fprintf(fptr, "Start test with packet %s\n", pcap_fname);

	/*
	 * Sending socket 1 
	 */
	sock1 = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (sock1 < 0)
		bail("socket");
	
	// set device timestamp
	memset(&device, 0, sizeof(device));
	strncpy(device.ifr_name, iface1, sizeof(device.ifr_name));
	
	// set hwtstamp features
	memset(&hwtstamp, 0, sizeof(hwtstamp));
	strncpy(hwtstamp.ifr_name, iface1, sizeof(hwtstamp.ifr_name));
	hwtstamp.ifr_data = (void *)&hwconfig;
	memset(&hwconfig, 0, sizeof(hwconfig));
	hwconfig.tx_type = HWTSTAMP_TX_ON;
	hwconfig.rx_filter = HWTSTAMP_FILTER_PTP_V1_L4_SYNC;

	if (ioctl(sock1, SIOCSHWTSTAMP, &hwtstamp) < 0) {
		bail("SIOCSHWTSTAMP");
	}
	
	if (setsockopt(sock1, SOL_SOCKET, SO_TIMESTAMPING,
			   &so_timestamping_flags,
			   sizeof(so_timestamping_flags)) < 0)
		bail("setsockopt SO_TIMESTAMPING");

	if((ioctl(sock1,SIOCGIFINDEX,&device))<0)
		bail("Err if index\n");
	
	struct sockaddr_ll sadr_ll;
	sadr_ll.sll_ifindex = device.ifr_ifindex; // index of iface1
	sadr_ll.sll_halen = ETH_ALEN; // length of destination mac address
	sadr_ll.sll_addr[5] = 0xff;
	sadr_ll.sll_addr[4] = 0xff;
	sadr_ll.sll_addr[3] = 0xff;
	sadr_ll.sll_addr[2] = 0xff;
	sadr_ll.sll_addr[1] = 0xff;
	sadr_ll.sll_addr[0] = 0xff;

	sadr_ll.sll_family = AF_PACKET; 
    	sadr_ll.sll_protocol = htons(ETH_P_ALL); 
	
	if ((bind(sock1, (struct sockaddr *)&sadr_ll , sizeof(sadr_ll))) ==-1)
    	{
        	perror("bind: ");
        	exit(-1);
    	}
	
	/* 
	 * Send packets forever every five seconds 
         */
	gettimeofday(&next, 0);
	next.tv_sec = (next.tv_sec + 1);
	next.tv_usec = 0;
	while (npkts < 30) {
		struct timeval now;
		struct timeval delta;
		long delta_us;
		int res;
		fd_set readfs1, errorfs1;
		gettimeofday(&now, 0);
		delta_us = (long)(next.tv_sec - now.tv_sec) * 1000000 +
			(long)(next.tv_usec - now.tv_usec);
		long recv_ts;
		long send_ts;
		if (delta_us > 0) {
			/* continue waiting for timeout or data */
			delta.tv_sec = 0;
			delta.tv_usec = 900000;

			FD_ZERO(&readfs1);
			FD_ZERO(&errorfs1);
			FD_SET(sock1, &readfs1);
			FD_SET(sock1, &errorfs1);

			printf("######### Send socket:\n");
			printf("%ld.%06ld: select 1 %ldus\n",
			       (long)now.tv_sec, (long)now.tv_usec,
			       delta_us);
			res = select(sock1 + 1, &readfs1, 0, &errorfs1, &delta);
			gettimeofday(&now, 0);
			printf("%ld.%06ld: select returned: %d, %s\n",
			       (long)now.tv_sec, (long)now.tv_usec, res,
			       res < 0 ? strerror(errno) : "success");

			if (res > 0) {
				if (FD_ISSET(sock1, &readfs1))
					printf("Send sock -> ready for reading\n");
				if (FD_ISSET(sock1, &errorfs1))
					printf("Send sock -> has error\n");
				send_ts = recvpacket(sock1, 0,
					   siocgstamp,
					   siocgstampns);
				recv_ts = recvpacket(sock1, MSG_ERRQUEUE,
					   siocgstamp,
					   siocgstampns);
                
                long delay = send_ts - recv_ts;

                if (delay > 0) {
                    green();
                    printf("\n\n\nDelay: %ldns\n\n\n", delay);
                    reset_color();
                    fprintf(fptr, "[%d] Delay: %ldns\n", npkts, delay);
                }
				else{
                    green();
                    printf("\n\n\nDelay: N.A.\n\n\n");
                    fprintf(fptr, "[%d] Delay: N.A.\n", npkts);
                    reset_color();
                }
			}
		} else {
			int sendlen =  sendto(sock1, pkt, pkt_len, 0,
						(const struct sockaddr*) &sadr_ll,
						sizeof(struct sockaddr_ll));
			printf("sendlen %d\n", sendlen);
			next.tv_sec += 1;
			npkts++;
			continue;
		}
	}
	fclose(fptr);
	return 0;
}

