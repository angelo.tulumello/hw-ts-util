all: build

build:
	gcc ts-send.c -o ts -lpcap

install: build
	cp ts /usr/local/bin/tulustamp

